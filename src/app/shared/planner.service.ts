import { Observable } from "rxjs";
import { BehaviorSubject } from "rxjs";
import { Day, RecipeDayInfo } from "./../../../interfaces";
import { Injectable } from "@angular/core";
import { AngularFirestore, DocumentSnapshot } from "@angular/fire/firestore";
import { Store } from "../store";
import { first, switchMap, tap } from "rxjs/operators";
import { forkJoin } from "rxjs";
import { Recipe } from "../../../interfaces";
import { AuthService } from "../auth/auth.service";

@Injectable({
  providedIn: "root",
})
export class PlannerService {
  startOfWeek$: BehaviorSubject<Date>;
  currDate$: BehaviorSubject<Date>;
  days$: Observable<Day>;
  dayId: string;

  constructor(
    private fs: AngularFirestore,
    private store: Store,
    private authServ: AuthService
  ) {
    this.startOfWeek$ = new BehaviorSubject(this.getStartOfWeek(new Date()));
    this.currDate$ = new BehaviorSubject(this.getInitalDate(new Date()));
    this.dayId = this.currDate$.getValue().getTime().toString();
    this.days$ = this.currDate$.pipe(
      switchMap((date: Date) => {
        return this.getDays(`${this.dayId}`);
      })
    );
  }

  createNewDay(day: Day): Promise<void> {
    return this.fs
      .collection("users")
      .doc(this.UID)
      .collection<Day[]>("days")
      .doc<Day>(day.id)
      .set(day);
  }

  weekChange(dir: string) {
    let sign = -1;
    if (dir === "right") sign = 1;
    const oldDate = this.startOfWeek$.getValue();
    const startOfWeek = new Date(
      oldDate.getFullYear(),
      oldDate.getMonth(),
      oldDate.getDate() + 7 * sign
    );
    this.startOfWeek$.next(startOfWeek);
    let currDate = this.currDate$.getValue();
    currDate.setDate(currDate.getDate() + 7 * sign);
    this.dayId = currDate.getTime().toString();
    this.currDate$.next(currDate);
  }

  dayChange(day: number) {
    const oldDate = this.startOfWeek$.getValue();
    const newDate = new Date(
      oldDate.getFullYear(),
      oldDate.getMonth(),
      oldDate.getDate() + day
    );
    this.dayId = newDate.getTime().toString();
    this.currDate$.next(newDate);
  }

  getStartOfWeek(date: Date) {
    const day = date.getDay();
    const diff = date.getDate() - day;
    date.setDate(diff);
    date.setHours(0, 0, 0, 0);
    return date;
  }
  getInitalDate(date: Date) {
    date.setHours(0, 0, 0, 0);
    return date;
  }

  getDays(id: string): Observable<Day> {
    let days = this.store.select<any>("days");
    days.pipe(first()).subscribe((dayKey) => {
      //check if day has already been queried
      if (!dayKey[id]) {
        this.fs
          .collection("users")
          .doc(this.UID)
          .collection("days")
          .doc(id)
          .ref.get()
          .then((docSnap) => {
            if (!docSnap.exists) {
              const emptyDay: Day = {
                breakfast: [],
                lunch: [],
                dinner: [],
                id,
              };
              this.createNewDay(emptyDay).then(() => {
                dayKey[id] = emptyDay;
                this.store.set("days", dayKey);
              });
            } else {
              const day: Day = docSnap.data() as Day;
              dayKey[id] = day;
              this.store.set("days", dayKey);
              //getting recipes
              this.getDayRecipes(day);
            }
          });
      }
    });

    return days;
  }

  addRecipesToStore(info: RecipeDayInfo, recipeKey: any) {
    let tasks = [];
    for (const key in info) {
      const elem = info[key];
      tasks.push(
        this.fs.collection("recipes").doc(elem.recipeID).get().pipe(first())
      );
    }

    const sub = forkJoin(...tasks).subscribe((docSnaps) => {
      for (let i = 0; i < docSnaps.length; i++) {
        const docSnap: DocumentSnapshot<Recipe> = docSnaps[i];
        if (docSnap.exists) {
          recipeKey[docSnap.id] = docSnap.data();
        } else {
          this.removeRecipeFromday(info[docSnap.id]);
        }
      }
      this.store.set("recipes", recipeKey);
      sub.unsubscribe();
    });
  }

  getDayRecipes(day: Day) {
    this.store
      .select("recipes")
      .pipe(first())
      .subscribe((recipeKey: any) => {
        let recipesToGet: RecipeDayInfo = {};
        for (const key in day) {
          if (key === "id") continue;

          //key is "breakfast", "lunch", "dinner"
          const recipeArr = day[key];

          for (let i = 0; i < recipeArr.length; i++) {
            const recipeId: string = recipeArr[i];

            if (recipeKey[recipeId]) {
            } else {
              recipesToGet[recipeId] = {
                dayId: day.id,
                cat: key,
                recipeID: recipeId,
              };
            }
          }
        }
        this.addRecipesToStore(recipesToGet, recipeKey);
      });
  }

  addRecipeToDay(recipeId: string) {
    //put recipe in store if not there
    this.store
      .select("recipes")
      .pipe(first())
      .subscribe((recipeKey: any) => {
        if (recipeKey[recipeId]) {
          return;
        } else {
          //grab from DB
          this.fs
            .collection("recipes")
            .doc(recipeId)
            .get()
            .pipe(first())
            .subscribe((recipe) => {
              recipeKey[recipeId] = recipe.data();
              this.store.set("recipes", recipeKey);
            });
        }
      });

    this.store
      .select("currChoosing")
      .pipe(first())
      .subscribe((val: any) => {
        this.store
          .select("days")
          .pipe(first())
          .subscribe((daysKey) => {
            let day: Day = daysKey[val.dayId];
            day[val.cat].push(recipeId);
            //upload to fs
            this.fs
              .collection("users")
              .doc(this.UID)
              .collection("days")
              .doc(val.dayId)
              .set(day);
            //upload to store
            daysKey[val.dayId] = day;
            this.store.set("days", daysKey);
          });
      });
  }

  removeRecipeFromday(removeData: {
    dayId: string;
    cat: string;
    recipeID: string;
  }) {
    this.store
      .select("days")
      .pipe(first())
      .subscribe((daysKey) => {
        let day: Day = daysKey[removeData.dayId];
        let arr: any[] = day[removeData.cat];
        let index = arr.indexOf(removeData.recipeID);
        arr.splice(index, 1);
        //upload to fs
        this.fs
          .collection("users")
          .doc(this.UID)
          .collection("days")
          .doc(removeData.dayId)
          .set(day);
        //upload to store
        daysKey[removeData.dayId] = day;
        this.store.set("days", daysKey);
      });
  }

  get UID(): string {
    return this.authServ.UID;
  }
}
