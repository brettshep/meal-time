import { Router, NavigationEnd } from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class UrlService {
  private history: string[] = [];
  private currentUrl: string = "";
  goingBack: boolean;
  constructor(private router: Router) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.currentUrl && !this.goingBack)
          this.history.unshift(this.currentUrl);
        this.currentUrl = event.url;
        this.goingBack = false;
      }
    });
  }

  get PreviousUrl() {
    return this.history.length ? this.history[0] : "";
  }
  get CurrentUrl() {
    return this.currentUrl;
  }

  go(route: string) {
    this.router.navigateByUrl(route);
  }

  back() {
    const prevURL: string = this.history.length
      ? this.history.splice(0, 1)[0]
      : "/planner";

    this.goingBack = true;
    this.router.navigateByUrl(prevURL);
  }
}
