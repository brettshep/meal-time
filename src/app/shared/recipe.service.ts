import { AngularFireStorage } from "@angular/fire/storage";
import { Recipe } from "./../../../interfaces";
import { Store } from "./../store";
import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { first } from "rxjs/operators";
import { AuthService } from "../auth/auth.service";

@Injectable({
  providedIn: "root",
})
export class RecipeService {
  constructor(
    private fs: AngularFirestore,
    private store: Store,
    private storage: AngularFireStorage,
    private authServ: AuthService
  ) {}

  addRecipe(recipe: Recipe) {
    const id = this.fs.createId();
    const obj = { ...recipe, id, owner: this.authServ.UID };
    this.fs.collection("recipes").doc(id).set(obj);
    //add to local store
    this.store
      .select("recipes")
      .pipe(first())
      .subscribe((recipeKey) => {
        recipeKey[id] = obj;
        this.store.set("recipes", recipeKey);
      });
  }

  editRecipe(recipe: Recipe) {
    this.fs.collection("recipes").doc(recipe.id).update(recipe);

    //add to local store
    this.store
      .select("recipes")
      .pipe(first())
      .subscribe((recipeKey) => {
        recipeKey[recipe.id] = recipe;
        this.store.set("recipes", recipeKey);
      });
  }

  deleteRecipe(recipe: Recipe) {
    //remove from fs
    this.fs.collection("recipes").doc(recipe.id).delete();
    //remove image from storage
    this.storage
      .ref(`/images/${recipe.pictureName}`)
      .delete()
      .toPromise()
      .catch((err) => console.error(err));
    //remove from local store
    this.store
      .select("recipes")
      .pipe(first())
      .subscribe((recipeKey) => {
        recipeKey[recipe.id] = undefined;
        this.store.set("recipes", recipeKey);
      });
  }

  getRecipeKey(id): Observable<Recipe> {
    let storeRecipes = this.store.select<any>("recipes");
    storeRecipes.pipe(first()).subscribe((recipeKey) => {
      //check if brief and correct id
      if (!recipeKey[id]) {
        this.fs
          .collection("recipes")
          .doc(id)
          .ref.get()
          .then((docSnapshot) => {
            if (!docSnapshot.exists) {
              console.error(`Document ${id} doesn't exist`);
            } else {
              const dbRecipe: Recipe = docSnapshot.data() as Recipe;
              recipeKey[id] = dbRecipe;
              this.store.set("recipes", recipeKey);
            }
          });
      }
    });
    return storeRecipes;
  }
}
