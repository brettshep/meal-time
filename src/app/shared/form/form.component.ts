import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChildren,
  ElementRef,
  QueryList
} from "@angular/core";
import { FormBuilder, Validators, FormGroup, FormArray } from "@angular/forms";
import { Recipe } from "interfaces";
import { AngularFireStorage } from "@angular/fire/storage";

@Component({
  selector: "recipe-form",
  template: `
    <div class="wrapper">
      <div
        class="coverImg"
        [ngStyle]="{ 'background-image': 'url(' + imageUrl + ')' }"
      ></div>
      <div class="textPadding">
        <section>
          <h2>
            IMAGE
            <span class="error" *ngIf="!imageUrl && submitted">
              <i class="fas fa-exclamation-triangle"></i>
            </span>
          </h2>
          <div class="uploadImageBtn input smallInput">
            Upload Image
            <input type="file" (change)="setFile($event)" />
          </div>
        </section>
        <form [formGroup]="form" (submit)="onSubmit()">
          <section>
            <h2>
              TITLE
              <span class="error" *ngIf="title.errors && submitted">
                <i class="fas fa-exclamation-triangle"></i>
              </span>
            </h2>
            <input
              type="text"
              class="input largeInput textField"
              id="title"
              name="title"
              formControlName="title"
              placeholder="Name Of Dish"
            />
          </section>
          <section>
            <h2>CATEGORY</h2>
            <div class="selectWrapper input smallInput">
              <select id="cat" name="cat" formControlName="cat">
                <!-- Default then ngfor -->
                <option *ngFor="let type of catTypes" [value]="type">{{
                  type
                }}</option>
              </select>
            </div>
          </section>
          <section>
            <h2>
              COOK TIME
            </h2>
            <div class="selectWrapper input smallInput">
              <select id="time" name="time" formControlName="time">
                <!-- Default then ngfor -->
                <option *ngFor="let time of times" [value]="time">{{
                  time
                }}</option>
              </select>
            </div>
          </section>
          <div class="seperator"></div>
          <section>
            <h2>
              URL
              <span class="error" *ngIf="url.errors && submitted">
                <i class="fas fa-exclamation-triangle"></i>
              </span>
            </h2>
            <input
              type="text"
              class="input largeInput textField"
              id="url"
              name="url"
              formControlName="url"
              placeholder="Recipe URL"
            />
          </section>
          <section>
            <h2 class="or">
              OR
            </h2>
          </section>
          <section formArrayName="ingredients">
            <h2>
              INGREDIENTS
              <span class="error" *ngIf="ingredients.errors && submitted">
                <i class="fas fa-exclamation-triangle"></i>
              </span>
            </h2>
            <div
              class="ingredient"
              *ngFor="let ingredient of ingredients.controls; index as i"
              [formGroupName]="i"
            >
              <input
                name="ingredient"
                formControlName="text"
                placeholder="Ingredient"
              />
              <div class="deleteIngredient" (click)="deleteIngredient(i)">
                <i class="fas fa-times"></i>
              </div>
            </div>
            <div class="input addIngredientBtn" (click)="addIngredient()">
              <i class="fas fa-plus"></i>
            </div>
          </section>
          <section>
            <h2>
              DIRECTIONS
              <span class="error" *ngIf="directions.errors && submitted">
                <i class="fas fa-exclamation-triangle"></i>
              </span>
            </h2>
            <textarea
              #textArea
              class="input largeInput"
              id="directions"
              name="directions"
              formControlName="directions"
              placeholder="Recipe Directions"
              (input)="resizeTextBox($event)"
            >
            </textarea>
          </section>
          <button class="submit" type="submit">{{ btnText }}</button>
          <!-- Form Errors On Submit-->
          <div
            class="formErrors"
            *ngIf="(!this.form.valid || urlOrError) && submitted"
          >
            Looks like your recipe has errors!
            <i class="fas fa-exclamation-triangle"></i>
          </div>
          <!-- Form Valid On Submit -->
          <div class="formValid" *ngIf="success">
            {{ submittedText }}
            <i class="fas fa-check-circle"></i>
          </div>
        </form>
      </div>
    </div>
  `,
  styleUrls: ["./form.component.sass"]
})
export class FormComponent implements OnInit {
  @Input()
  recipeData: Recipe;
  @Output()
  recipeOut = new EventEmitter<Recipe>();
  @ViewChildren("textArea")
  textAreas: QueryList<ElementRef>;
  form: FormGroup;
  submitted: boolean = false;
  success: boolean = false;
  imageFile: File | Blob = null;
  imageUrl: string = "";
  catTypes: string[] = [
    "Beef",
    "Chicken",
    "Pork",
    "Fish",
    "Vegetables",
    "Sweets",
    "Other"
  ];
  times: string[] = [
    "5min",
    "10min",
    "15min",
    "20min",
    "25min",
    "30min",
    "35min",
    "40min",
    "45min",
    "50min",
    "55min",
    "1hr",
    "2hr",
    "3hr",
    "4hr",
    "5hr",
    "6hr",
    "7hr+"
  ];
  urlOrError = false;

  constructor(private fb: FormBuilder, private storage: AngularFireStorage) {}

  ngOnInit() {
    //form
    this.form = this.fb.group({
      title: ["", [Validators.required]],
      time: ["5min", [Validators.required]],
      cat: ["Beef", [Validators.required]],
      url: ["", []],
      ingredients: this.fb.array([]),
      directions: ["", []]
    });

    if (this.recipeData) {
      this.setDefaults();
    }
  }

  ngAfterViewInit() {
    this.textAreas.forEach(area => {
      this.resizeTextBox(null, area.nativeElement);
    });
  }

  setDefaults() {
    for (const key in this.recipeData) {
      //this.briefData[key];
      const formControl = this.form.get(key);
      //add arrays
      if (formControl) {
        if (key === "ingredients") {
          this.recipeData["ingredients"].forEach(val =>
            this.ingredients.push(this.createIngredientFilled(val.text))
          );
        }
        if (key === "directions") {
          formControl.setValue(
            this.recipeData["directions"].replace(/<br\/>/g, "\n")
          );
        } else {
          formControl.setValue(this.recipeData[key]);
        }
      }
      //picture url
      else if (key === "picture") {
        this.imageUrl = this.recipeData["picture"];
      }
    }
  }

  setFile(e: any) {
    const files = e.target.files;
    if (files && files[0]) {
      const file: File = files[0];
      const regex = /jpeg|png|gif/;
      //see if image file
      let fileType = file.type.match(regex);
      if (fileType.length) {
        this.imageFile = file;
        this.resizeImage({ maxSize: 1000, file, quality: 0.7 }).then(blob => {
          this.imageUrl = URL.createObjectURL(blob);
          this.imageFile = blob;
        });
      }
    }
  }

  //--------FORM SUBMIT------
  onSubmit() {
    this.submitted = true;
    if (!this.form.valid || this.success) {
      return;
    }
    if (
      !this.url.value &&
      (!this.directions.value || !this.ingredients.length)
    ) {
      this.urlOrError = true;
      return;
    } else {
      this.urlOrError = false;
    }

    //if uploading new image, aka there is an imgfile
    if (this.imageFile) {
      let name = Date.now();
      this.storage
        .upload(`images/${name}`, this.imageFile)
        .then(a => a.ref.getDownloadURL())
        .then(url => {
          //if editing recipe,make sure to add id
          if (this.recipeData) {
            this.recipeOut.emit({
              ...this.form.value,
              id: this.recipeData.id,
              picture: url,
              pictureName: name,
              directions: this.directions.value.replace(/\n/g, "<br/>")
            });
          }
          //if new recipe, id will be added uplon firestore upload
          else {
            this.recipeOut.emit({
              ...this.form.value,
              picture: url,
              pictureName: name,
              directions: this.directions.value.replace(/\n/g, "<br/>")
            });
          }
        });
    }
    //if not uploading new image
    else {
      //brief would have to be being edited
      this.recipeOut.emit({
        ...this.form.value,
        id: this.recipeData.id,
        picture: this.recipeData.picture,
        pictureName: this.recipeData.pictureName,
        directions: this.directions.value.replace(/\n/g, "<br/>")
      });
    }
    this.success = true;
  }

  //--------TEXTBOX SIZING------
  resizeTextBox(e: Event, elem?: HTMLTextAreaElement) {
    let textElem: HTMLTextAreaElement;
    if (e) {
      textElem = e.target as HTMLTextAreaElement;
    } else {
      textElem = elem;
    }
    if (textElem.scrollHeight > textElem.clientHeight) {
      textElem.style.height = textElem.scrollHeight + "px";
    } else {
      textElem.style.height = 0 + "px";
      textElem.style.height = textElem.scrollHeight + "px";
    }
  }

  //--------INGREDIENTS------
  deleteAsset(i: number) {
    this.ingredients.removeAt(i);
  }

  createIngredientFilled(e: string): FormGroup {
    return this.fb.group({
      text: e
    });
  }

  createIngredient(): FormGroup {
    return this.fb.group({
      text: ""
    });
  }

  addIngredient() {
    this.ingredients.push(this.createIngredient());
  }

  deleteIngredient(i: number) {
    this.ingredients.removeAt(i);
  }

  //--------GETTERS------
  get title() {
    return this.form.get("title");
  }
  get time() {
    return this.form.get("time");
  }
  get cat() {
    return this.form.get("cat");
  }
  get url() {
    return this.form.get("url");
  }
  get ingredients(): FormArray {
    return this.form.get("ingredients") as FormArray;
  }
  get directions() {
    return this.form.get("directions");
  }

  get btnText() {
    return this.recipeData ? "Confirm" : "Add Recipe";
  }

  get submittedText() {
    return this.recipeData
      ? "Confirmed Successfully"
      : "Submitted Successfully";
  }

  //---------UTILITIES-----------
  //resize image
  resizeImage(settings: {
    maxSize: number;
    quality: number;
    file: File | Blob;
  }): Promise<Blob> {
    const file = settings.file;
    const maxSize = settings.maxSize;
    const reader = new FileReader();
    const image = new Image();
    const canvas = document.createElement("canvas");
    const dataURItoBlob = (dataURI: string) => {
      const bytes =
        dataURI.split(",")[0].indexOf("base64") >= 0
          ? atob(dataURI.split(",")[1])
          : unescape(dataURI.split(",")[1]);
      const mime = dataURI
        .split(",")[0]
        .split(":")[1]
        .split(";")[0];
      const max = bytes.length;
      const ia = new Uint8Array(max);
      for (var i = 0; i < max; i++) ia[i] = bytes.charCodeAt(i);
      return new Blob([ia], { type: mime });
    };
    const resize = () => {
      let width = image.width;
      let height = image.height;

      if (width > height) {
        height *= maxSize / width;
        width = maxSize;
      } else {
        width *= maxSize / height;
        height = maxSize;
      }
      canvas.width = width;
      canvas.height = height;
      let context = canvas.getContext("2d");
      context.drawImage(image, 0, 0, width, height);
      let dataUrl = canvas.toDataURL("image/jpeg", settings.quality);
      return dataURItoBlob(dataUrl);
    };

    return new Promise((ok, no) => {
      if (!file.type.match(/image.*/)) {
        no(new Error("Not an image"));
        return;
      }

      reader.onload = (readerEvent: any) => {
        image.onload = () => ok(resize());
        image.src = readerEvent.target.result;
      };
      reader.readAsDataURL(file);
    });
  }
}
