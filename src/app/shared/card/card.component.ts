import { Recipe } from "./../../../../interfaces";
import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "card",
  template: `
    <ng-container>
      <div class="wrapper" *ngIf="recipe; else upload">
        <img [src]="recipe.picture" alt="food" />
        <h3>{{ recipe.title }}</h3>
        <div class="specs">
          <span>
            <i class="far fa-clock"></i>
            {{ recipe.time }}
          </span>
        </div>
        <ng-content select=".cardHover"></ng-content>
      </div>
    </ng-container>

    <ng-template #upload>
      <div class="plus">
        <i class="fas fa-plus"></i>
      </div>
    </ng-template>
  `,
  styleUrls: ["./card.component.sass"],
})
export class CardComponent {
  @Input()
  recipe: Recipe;
  @Input()
  delete: boolean;
}
