import { CompName } from "./routing.module";
import {
  trigger,
  transition,
  group,
  query,
  style,
  animate,
} from "@angular/animations";

let up = [
  query(
    ":enter",
    style({
      transform: "translateY(100%)",
      zIndex: 100,
      boxShadow: "0 -10px 30px 0 rgba(0,0,0,.1)",
    })
  ),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),
  query(":enter", [
    animate(".5s ease", style({ transform: "translateY(0%)" })),
  ]),
];

let down = [
  query(
    ":leave",
    style({ zIndex: 100, boxShadow: "0 -10px 30px 0 rgba(0,0,0,.1)" })
  ),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),

  query(":leave", [
    animate(".5s ease", style({ transform: "translateY(100%)" })),
  ]),
];

let right = [
  query(":enter", style({ transform: "translateX(100%)" })),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),
  group([
    query(
      ":leave",
      [animate(".5s ease", style({ transform: "translateX(-100%)" }))],
      { optional: true }
    ),
    query(":enter", [
      animate(".5s ease", style({ transform: "translateX(0%)" })),
    ]),
  ]),
];

let left = [
  query(":enter", style({ transform: "translateX(-100%)" })),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),
  group([
    query(
      ":leave",
      [animate(".5s ease", style({ transform: "translateX(100%)" }))],
      { optional: true }
    ),
    query(":enter", [
      animate(".5s ease", style({ transform: "translateX(0%)" })),
    ]),
  ]),
];

export const RouteAnimations = [
  trigger("routeAnimation", [
    transition(`-1 => ${CompName.planner}`, []),

    // // // ** TO EXPLORE
    // transition(`* => ${CompName.explore}`, up),
    // // // ** FROM EXPLROE
    // transition(`${CompName.explore} => *`, down),

    // // // ** TO EDITOR
    // transition(`* => ${CompName.editor}`, right),
    // // // ** FROM EDITOR
    // transition(`${CompName.editor} => *`, left),

    // // // ** TO RECIPE
    // transition(`* => ${CompName.recipe}`, right),
    // // // ** FROM RECIPE
    // transition(`${CompName.recipe} => *`, left)
  ]),
];
