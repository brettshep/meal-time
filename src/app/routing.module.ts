import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./auth/authGuard.guard";

export enum CompName {
  planner = 1,
  recipe,
  editor,
  explore,
}

const routes: Routes = [
  {
    path: "auth",
    loadChildren: "./auth/auth.module#AuthModule",
  },
  {
    canActivate: [AuthGuard],
    path: "planner",
    data: { name: CompName.planner },
    loadChildren: "./planner/planner.module#PlannerModule",
  },
  {
    canActivate: [AuthGuard],
    path: "recipe",
    data: { name: CompName.recipe },
    loadChildren: "./recipe/recipe.module#RecipeModule",
  },
  {
    canActivate: [AuthGuard],
    path: "editor",
    data: { name: CompName.editor },
    loadChildren: "./editor/editor.module#EditorModule",
  },
  {
    canActivate: [AuthGuard],
    path: "explore",
    data: { name: CompName.explore },
    loadChildren: "./explore/explore.module#ExploreModule",
  },
  {
    canActivate: [AuthGuard],
    path: "terms",
    loadChildren:
      "./terms-conditions/terms-conditions.module#TermsConditionsModule",
  },
  {
    canActivate: [AuthGuard],
    path: "privacy-policy",
    loadChildren: "./privacy-policy/privacy-policy.module#PrivacyPolicyModule",
  },
  { path: "**", redirectTo: "auth" },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
