import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";
import { Recipe, Day } from "../../../../interfaces";
import { Store } from "../../store";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "choosen-meals",
  template: `
  <ng-container *ngIf="(recipeList$ | async) as list; else loading">
    <div class="wrapper">
    <!----------- BREAKFAST ------------>
      <div class="timeOfDay">
        <h3>BREAKFAST</h3>
        <ng-container *ngIf="day?.breakfast.length; else emptyBreak">
          <div class="mealCard" *ngFor="let id of day.breakfast" >
            <card 
            *ngIf="list[id]; else deleteCard"
            [recipe]="list[id]"
            >
              <div class="cardHover">
                <span (click)="viewRecipe(id)">
                  <i class="fas fa-book"></i>
                  View Recipe
                </span>
                <span (click)="removeRecipe('breakfast', id)">
                  <i class="fas fa-times"></i>
                  Remove Recipe
                </span>
              </div>
            </card>
            </div>
            <div class="addCard" (click)="emitNew('breakfast')">
              <i class="fas fa-plus"></i>
            </div>
        </ng-container>
      </div>
    <!----------- LUNCH ------------>
      <div class="timeOfDay">
        <h3>LUNCH</h3>
        <ng-container *ngIf="day?.lunch.length; else emptyLunch">
        <div class="mealCard" *ngFor="let id of day.lunch" >
            <card 
            *ngIf="list[id]; else deleteCard"
            [recipe]="list[id]"
            >
            <div class="cardHover">
                <span (click)="viewRecipe(id)">
                  <i class="fas fa-book"></i>
                  View Recipe
                </span>
                <span (click)="removeRecipe('lunch',id)">
                  <i class="fas fa-times"></i>
                  Remove Recipe
                </span>
              </div>
            </card>
            </div>
            <div class="addCard" (click)="emitNew('lunch')">
              <i class="fas fa-plus"></i>
            </div>
          </ng-container>
      </div>
  <!----------- DINNER ------------>
      <div class="timeOfDay">
        <h3>DINNER</h3>
        <ng-container *ngIf="day?.dinner.length; else emptyDinner" >
          <div class="mealCard" *ngFor="let id of day.dinner" >
            <card 
            *ngIf="list[id]; else deleteCard"
            [recipe]="list[id]"
            >
            <div class="cardHover">
                <span (click)="viewRecipe(id)">
                  <i class="fas fa-book"></i>
                  View Recipe
                </span>
                <span (click)="removeRecipe('dinner',id)">
                  <i class="fas fa-times"></i>
                  Remove Recipe
                </span>
              </div>
            </card>
            </div>
            <div class="addCard" (click)="emitNew('dinner')">
              <i class="fas fa-plus"></i>
            </div>
          </ng-container>
      </div>

      <ng-template #emptyBreak>
        <div class="mealCard">
          <card (click)="emitNew('breakfast')"></card>
        </div>
      </ng-template>
      <ng-template #emptyLunch>
        <div class="mealCard">
          <card (click)="emitNew('lunch')"></card>
        </div>
      </ng-template>
      <ng-template #emptyDinner>
        <div class="mealCard">
          <card (click)="emitNew('dinner')"></card>
        </div>
      </ng-template>

      <ng-template #deleteCard>
          <card [delete]="true"></card>
      </ng-template>


    </div>
  </ng-container>
  <ng-template #loading>Loading</ng-template>
  `,
  styleUrls: ["./choosen-meals.component.sass"]
})
export class ChoosenMealsComponent {
  constructor(private store: Store) {}

  recipeList$: any;
  ngOnInit() {
    this.recipeList$ = this.store.select("recipes");
  }

  @Input()
  day: Day;

  @Output()
  newRecipe: EventEmitter<{ dayId: string; cat: string }> = new EventEmitter();

  @Output()
  removeRecipeEmit: EventEmitter<{
    dayId: string;
    cat: string;
    recipeId: string;
  }> = new EventEmitter();

  @Output()
  viewRecipeEmit: EventEmitter<string> = new EventEmitter();

  emitNew(cat: string) {
    this.newRecipe.emit({ cat, dayId: this.day.id });
  }

  viewRecipe(id: string) {
    this.viewRecipeEmit.emit(id);
  }

  removeRecipe(cat: string, id: string) {
    this.removeRecipeEmit.emit({ cat, dayId: this.day.id, recipeId: id });
  }
}
