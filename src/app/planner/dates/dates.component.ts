import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "dates",
  template: `
  <div class="wrapper">
    <h3>{{start | date: 'MMMM d'}} - {{end | date: 'MMMM d'}}</h3>
    <div class="daysCont">
      <button class="arrow" (click)="move('left')">
        <img src="/assets/chevron.png" alt="arrow">
      </button>
      <div class="days">
        <button (click)="dayChange(0)" [class.active]="day === 0">Su</button>
        <button (click)="dayChange(1)" [class.active]="day === 1">M</button>
        <button (click)="dayChange(2)" [class.active]="day === 2">T</button>
        <button (click)="dayChange(3)" [class.active]="day === 3">W</button>
        <button (click)="dayChange(4)" [class.active]="day === 4">Th</button>
        <button (click)="dayChange(5)" [class.active]="day === 5">F</button>
        <button (click)="dayChange(6)" [class.active]="day === 6">S</button>
      </div>
      <button class="arrow" (click)="move('right')">
      <img src="/assets/chevron.png" alt="arrow">
      </button>
    </div>
  </div>
  `,
  styleUrls: ["./dates.component.sass"]
})
export class DatesComponent {
  start: Date;
  end: Date;
  day: number = 0;

  @Input()
  set dateStart(value) {
    this.start = value;
    this.end = new Date(
      value.getFullYear(),
      value.getMonth(),
      value.getDate() + 6
    );
  }

  @Input()
  set setDay(value: Date) {
    this.day = Math.round((value.getTime() - this.start.getTime()) / 86400000);
  }

  @Output()
  dateChange = new EventEmitter<string>();

  @Output()
  daySelect = new EventEmitter<number>();

  move(event) {
    this.dateChange.emit(event);
  }

  dayChange(event) {
    this.daySelect.emit(event);
  }
}
