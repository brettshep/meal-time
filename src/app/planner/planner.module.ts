import { CardModule } from "./../shared/card/card.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RouterModule, Routes } from "@angular/router";
import { PlannerComponent } from "./planner.component";
import { DatesComponent } from "./dates/dates.component";
import { ChoosenMealsComponent } from "./choosen-meals/choosen-meals.component";

export const ROUTES: Routes = [
  {
    path: "",
    component: PlannerComponent
  },
  { path: "**", redirectTo: "/planner" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), CardModule],
  declarations: [PlannerComponent, DatesComponent, ChoosenMealsComponent]
})
export class PlannerModule {}
