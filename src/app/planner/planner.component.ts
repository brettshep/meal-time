import { Store } from "./../store";
import { Day, User } from "./../../../interfaces";
import { Component, OnInit } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { PlannerService } from "../shared/planner.service";
import { UrlService } from "../shared/url.service";
import { map } from "rxjs/operators";

@Component({
  selector: "planner",
  template: `
    <div class="wrapper" *ngIf="user$ | async as user; else loading">
      <dates
        [dateStart]="startOfWeek"
        [setDay]="currDate"
        (dateChange)="weekChange($event)"
        (daySelect)="dayChange($event)"
      ></dates>
      <choosen-meals
        [day]="day$ | async"
        (newRecipe)="newRecipe($event)"
        (removeRecipeEmit)="removeRecipe($event)"
        (viewRecipeEmit)="viewRecipe($event)"
      ></choosen-meals>
    </div>
    <ng-template #loading>
      <div style="text-align: center;">Loading</div>
    </ng-template>
  `,
  styleUrls: ["./planner.component.sass"],
})
export class PlannerComponent implements OnInit {
  constructor(
    private planServ: PlannerService,
    private urlServ: UrlService,
    private store: Store
  ) {}
  startOfWeek$: BehaviorSubject<Date>;
  currDate$: BehaviorSubject<Date>;
  day$: Observable<Day>;
  user$: Observable<User>;

  ngOnInit() {
    this.user$ = this.store.select<User>("user");
    this.startOfWeek$ = this.planServ.startOfWeek$;
    this.currDate$ = this.planServ.currDate$;
    this.day$ = this.planServ.days$.pipe(
      map((dayKey) => dayKey[this.planServ.dayId])
    );
  }

  newRecipe(chooseData: { dayId: string; cat: string }) {
    //set currchoosing in store
    this.store.set("currChoosing", chooseData);
    this.urlServ.go("explore/select");
  }

  removeRecipe(removeData: { dayId: string; cat: string; recipeID: string }) {
    this.planServ.removeRecipeFromday(removeData);
  }

  weekChange(dir: string) {
    this.planServ.weekChange(dir);
  }

  dayChange(day: number) {
    this.planServ.dayChange(day);
  }

  viewRecipe(id: string) {
    this.urlServ.go(`/recipe/${id}`);
  }

  get startOfWeek() {
    return this.startOfWeek$.getValue();
  }
  get currDate() {
    return this.currDate$.getValue();
  }
}
