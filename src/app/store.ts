import { Observable, BehaviorSubject } from "rxjs";
import { pluck, distinctUntilChanged } from "rxjs/operators";
import { Recipe, Day } from "../../interfaces";

export interface State {
  // [key: string]: any;
  currRecipe: Recipe;
  recipes: {
    [id: string]: Recipe;
  };
  days: {
    [id: string]: Day;
  };
  currDate: Date;
  currChoosing: { dayId: string; cat: string };
}

const state: State = {
  currRecipe: undefined,
  days: {},
  recipes: {},
  currDate: undefined,
  currChoosing: undefined
};

export class Store {
  private subject = new BehaviorSubject<State>(state);
  private store = this.subject.asObservable().pipe(distinctUntilChanged());

  get value() {
    return this.subject.value;
  }

  select<T>(name: string): Observable<T> {
    return this.store.pipe(pluck(name));
  }

  set(name: string, state: any) {
    this.subject.next({ ...this.value, [name]: state });
    // console.log("STORE VALUE");
    // console.log(this.value);
  }
}
