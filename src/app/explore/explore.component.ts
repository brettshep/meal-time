import { first } from "rxjs/operators";
import { Store } from "./../store";
import { PlannerService } from "./../shared/planner.service";
import { ActivatedRoute } from "@angular/router";
import { UrlService } from "./../shared/url.service";
import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth/auth.service";

@Component({
  selector: "explore",
  template: `
    <div class="choosingBanner" *ngIf="selecting">Choose a Recipe</div>
    <div class="bg">
      <div class="wrapper">
        <ais-instantsearch [config]="searchConfig">
          <ais-configure
            [searchParameters]="{ hitsPerPage: 15 }"
          ></ais-configure>
          <ais-search-box placeholder="Search Recipes..."></ais-search-box>
          <div class="personalToggle">
            <span
              class="checkBox"
              [class.checked]="personalRecipesToggle"
              (click)="personalRecipesToggle = !personalRecipesToggle"
            ></span>
            Show Only My Recipes
          </div>
          <ais-hits>
            <ng-template let-hits="hits">
              <div
                *ngFor="
                  let hit of hits | personalRecipes: personalRecipesToggle:uid
                "
              >
                <card [recipe]="hit">
                  <div class="cardHover">
                    <span *ngIf="!selecting" (click)="goToRecipe(hit.id)">
                      <i class="fas fa-book"></i>
                      View Recipe
                    </span>
                    <span *ngIf="selecting" (click)="chooseRecipe(hit.id)">
                      <i class="fas fa-check-circle"></i>
                      Choose Recipe
                    </span>
                  </div>
                </card>
              </div>
            </ng-template>
          </ais-hits>
          <ais-pagination
            [padding]="1"
            [showLast]="true"
            (pageChanged)="pageChange()"
          ></ais-pagination>
        </ais-instantsearch>
        <div *ngIf="selecting" class="exitBtn" (click)="goBack()">
          <i class="fas fa-arrow-left"></i>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./explore.component.sass"],
})
export class ExploreComponent implements OnInit {
  selecting: boolean = false;
  searchConfig = {
    appId: "KL1Y2PU4TI",
    apiKey: "2470f243dc49af75affc25cf9599ff3d",
    indexName: "Main",
  };

  personalRecipesToggle: boolean = false;

  constructor(
    private urlServ: UrlService,
    private route: ActivatedRoute,
    private planServ: PlannerService,
    private store: Store,
    private authServ: AuthService
  ) {}

  ngOnInit() {
    //if selecting recipe for day
    if (!!this.route.snapshot.url.length) {
      this.selecting = true;
    }

    //check store if choosing, if not and selecting is true, navigate back to not selecting
    this.store
      .select("currChoosing")
      .pipe(first())
      .subscribe((val) => {
        if (!val && this.selecting) {
          this.urlServ.go("/explore");
        }
      });
  }

  get uid() {
    return this.authServ.UID;
  }

  pageChange() {
    window.scrollTo(0, 0);
  }

  goBack() {
    this.urlServ.go(`/planner`);
  }

  chooseRecipe(id: string) {
    this.planServ.addRecipeToDay(id);
    this.urlServ.go(`/planner`);
  }

  goToRecipe(id: string) {
    this.urlServ.go(`/recipe/${id}`);
  }
}
