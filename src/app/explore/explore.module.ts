import { CardModule } from "./../shared/card/card.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ExploreComponent } from "./explore.component";
import { RouterModule, Routes } from "@angular/router";
import { NgAisModule } from "angular-instantsearch";
import { PersonalRecipesPipe } from './personal-recipes.pipe';

export const ROUTES: Routes = [
  { path: "", pathMatch: "full", component: ExploreComponent },
  { path: "select", component: ExploreComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    NgAisModule,
    CardModule
  ],
  declarations: [ExploreComponent, PersonalRecipesPipe]
})
export class ExploreModule {}
