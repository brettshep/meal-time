import { Pipe, PipeTransform } from "@angular/core";
import { Recipe } from "../../../interfaces";

@Pipe({
  name: "personalRecipes",
})
export class PersonalRecipesPipe implements PipeTransform {
  transform(items: Recipe[], filter: boolean, uid: string): any {
    if (filter) {
      return items.filter((item) => item.owner === uid);
    } else {
      return items;
    }
  }
}
