import { Component } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import { RouteAnimations } from "./routeAnimations";
import { AuthService } from "./auth/auth.service";

@Component({
  selector: "app-root",
  template: `
    <header>
      <div class="desktop">
        <div class="left">
          <a routerLink="/planner">Planner</a>
          <a routerLink="/explore">Explore</a>
        </div>
        <h1>Meal Time</h1>
        <div class="right">
          <a routerLink="/editor/add"
            ><button><i class="fas fa-plus"></i> Add Meal</button></a
          >
        </div>
      </div>
      <div class="mobile">
        <i class="fas fa-bars" (click)="setBar()"></i>
        <h1>Meal Time</h1>
      </div>
    </header>
    <div class="sideBar" [class.active]="barOpen">
      <div class="exit" (click)="setBar()">
        <i class="fas fa-times"></i>
      </div>
      <a (click)="setBar()" routerLink="/planner">Planner</a>
      <a (click)="setBar()" routerLink="/explore">Explore</a>
      <a (click)="setBar()" routerLink="/editor/add"
        ><button><i class="fas fa-plus"></i> Add Meal</button></a
      >
    </div>
    <div class="page" [@routeAnimation]="getDepth(myOutlet)">
      <router-outlet #myOutlet="outlet"></router-outlet>
    </div>
  `,
  styleUrls: ["./app.component.sass"],
  animations: RouteAnimations,
})
export class AppComponent {
  barOpen: boolean = false;

  constructor(private authServ: AuthService) {}

  ngOnInit() {
    this.authServ.user$.subscribe();
  }

  setBar() {
    if (this.barOpen) {
      // document.body.style.overflow = "auto";
      this.barOpen = false;
    } else {
      // document.body.style.overflow = "hidden";
      this.barOpen = true;
    }
  }

  //-------ROUTER ANIMS------
  getDepth(outlet: RouterOutlet) {
    let name = outlet.activatedRouteData["name"];
    if (name) return name;
    else return -1;
  }
}
