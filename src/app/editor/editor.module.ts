import { RecipeFormModule } from "./../shared/form/form.module";

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { EditorComponent } from "./editor.component";

export const ROUTES: Routes = [
  { path: "", pathMatch: "full", redirectTo: "add" },
  { path: "add", component: EditorComponent },
  { path: "edit/:id", component: EditorComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), RecipeFormModule],
  declarations: [EditorComponent]
})
export class EditorModule {}
