import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { UrlService } from "../shared/url.service";
import { Component } from "@angular/core";
import { RecipeService } from "../shared/recipe.service";
import { Recipe } from "interfaces";
import { map } from "rxjs/operators";

@Component({
  selector: "editor",
  template: `
    <div class="exitBtn" (click)="goBack()">
      <i class="fas fa-arrow-left"></i>
    </div>
    <div class="wrapper">
      <ng-container *ngIf="editing">
        <ng-container *ngIf="recipe$ | async as recipe; else loading">
          <recipe-form
            [recipeData]="recipe"
            (recipeOut)="handleRecipe($event)"
          ></recipe-form>
        </ng-container>
      </ng-container>

      <ng-container *ngIf="!editing">
        <recipe-form (recipeOut)="handleRecipe($event)"></recipe-form>
      </ng-container>

      <ng-template #loading>
        Loading
      </ng-template>
    </div>
  `,
  styleUrls: ["./editor.component.sass"],
})
export class EditorComponent {
  constructor(
    private urlServ: UrlService,
    private recipeServ: RecipeService,
    private route: ActivatedRoute
  ) {}

  recipe$: Observable<Recipe>;
  editing: boolean = false;

  ngOnInit() {
    let id = this.route.snapshot.params["id"];
    //is editing recipe
    if (id) {
      this.editing = true;
      this.recipe$ = this.recipeServ
        .getRecipeKey(id)
        .pipe(map((recipeKey) => recipeKey[id]));
    }
  }

  handleRecipe(recipe: Recipe) {
    //editing recipe
    if (recipe.id) {
      this.recipeServ.editRecipe(recipe);
      //adding recipe
    } else {
      this.recipeServ.addRecipe(recipe);
    }
  }

  goBack() {
    this.urlServ.go(`explore`);
  }
}
