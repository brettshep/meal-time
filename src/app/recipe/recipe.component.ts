import { UrlService } from "./../shared/url.service";
import { Observable } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { Recipe } from "../../../interfaces";
import { ActivatedRoute } from "@angular/router";
import { RecipeService } from "../shared/recipe.service";
import { map } from "rxjs/operators";

@Component({
  selector: "recipe",
  template: `
    <div class="bg">
      <div class="wrapper">
        <div class="exitBtn" (click)="goBack()">
          <i class="fas fa-arrow-left"></i>
        </div>
        <ng-container *ngIf="recipe$ | async as recipe; else loading">
          <!-- IMAGE -->
          <div
            class="coverImg"
            [ngStyle]="{ 'background-image': 'url(' + recipe.picture + ')' }"
          >
            <div class="hover">
              <span (click)="editRecipe()">
                <i class="fas fa-edit"></i>
                Edit Recipe
              </span>
              <span (click)="deleteRecipe(recipe)">
                <i class="fas fa-trash-alt"></i>
                Delete Recipe
              </span>
            </div>
          </div>
          <div class="textPadding">
            <!-- TITLE -->
            <h1 class="title">
              {{ recipe.title }}
              <span>
                <i class="far fa-clock"></i>
                {{ recipe.time }}
              </span>
            </h1>

            <!-- LINE -->
            <div class="line"></div>

            <ng-container *ngIf="recipe.url; else ingredients">
              <section class="section">
                <h2>URL</h2>
                <a class="recipeURL" [href]="recipe.url" target="_blank">{{
                  recipe.url
                }}</a>
              </section>
            </ng-container>

            <!-- INGREDIENTS -->
            <ng-template #ingredients>
              <section class="ingredients section">
                <h2>INGREDIENTS</h2>
                <ul>
                  <li *ngFor="let ingredient of recipe.ingredients">
                    <span></span>
                    {{ ingredient.text }}
                  </li>
                </ul>
              </section>

              <!-- DIRECTIONS -->
              <section class="directions section">
                <h2>DIRECTIONS</h2>
                <p [innerHTML]="recipe.directions"></p>
              </section>
            </ng-template>
          </div>
        </ng-container>
        <ng-template #loading>Loading</ng-template>
      </div>
    </div>
  `,
  styleUrls: ["./recipe.component.sass"],
})
export class RecipeComponent implements OnInit {
  recipe$: Observable<Recipe>;
  id: string;
  constructor(
    private route: ActivatedRoute,
    private recipeServ: RecipeService,
    private urlServ: UrlService
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];
    this.recipe$ = this.recipeServ
      .getRecipeKey(this.id)
      .pipe(map((recipeKey) => recipeKey[this.id]));
  }

  editRecipe() {
    this.urlServ.go(`editor/edit/${this.id}`);
  }

  deleteRecipe(recipe: Recipe) {
    this.recipeServ.deleteRecipe(recipe);
    this.urlServ.go(`planner`);
  }

  goBack() {
    this.urlServ.go(`explore`);
  }
}
