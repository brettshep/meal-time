import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RecipeComponent } from "./recipe.component";
import { RouterModule, Routes } from "@angular/router";

export const ROUTES: Routes = [
  {
    path: ":id",
    component: RecipeComponent
  },
  { path: "**", redirectTo: "/planner" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [RecipeComponent]
})
export class RecipeModule {}
