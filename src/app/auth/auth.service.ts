import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestoreDocument,
  AngularFirestore,
} from "@angular/fire/firestore";
import { switchMap, tap } from "rxjs/operators";
import { of } from "rxjs";
import { Store } from "../store";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(
    private auth: AngularFireAuth,
    private fs: AngularFirestore,
    private store: Store
  ) {}

  user$ = this.auth.authState.pipe(
    switchMap((user) => {
      if (user) {
        return this.fs.doc(`users/${user.uid}`).valueChanges();
      } else {
        return of(null);
      }
    }),
    tap((user) => {
      this.store.set("user", user);
    })
  );

  LoginUser(email: string, password: string) {
    return this.auth.auth.signInWithEmailAndPassword(email, password);
  }

  Logout() {
    this.auth.auth.signOut();
  }

  CreateUser(email: string, password: string) {
    return this.auth.auth
      .createUserWithEmailAndPassword(email, password)
      .then((credential) => {
        //set default data if new user
        this.SetUserData(credential.user);
      });
  }

  //update user info in Firestore
  private SetUserData(user: firebase.User) {
    const userRef: AngularFirestoreDocument<any> = this.fs.doc(
      `users/${user.uid}`
    );
    const data = {
      uid: user.uid,
      email: user.email,
    };
    return userRef.set(data, { merge: true });
  }

  get authState() {
    return this.auth.authState;
  }

  get UID() {
    return this.auth.auth.currentUser.uid;
  }
}
