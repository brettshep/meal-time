import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
import { AppRoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Store } from "./store";

import { AngularFireModule, FirebaseAppConfig } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";

export const config: FirebaseAppConfig = {
  apiKey: "AIzaSyA4gkOWPEWWUPzqViCXwqybVu97aiWTCQ0",
  authDomain: "meal-time-e229e.firebaseapp.com",
  databaseURL: "https://meal-time-e229e.firebaseio.com",
  projectId: "meal-time-e229e",
  storageBucket: "meal-time-e229e.appspot.com",
  messagingSenderId: "376391292997",
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production,
    }),
  ],
  providers: [Store],
  bootstrap: [AppComponent],
})
export class AppModule {}
