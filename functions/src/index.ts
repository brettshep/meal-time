import * as functions from "firebase-functions";
import * as algolia from "algoliasearch";

//vars
const appID = "KL1Y2PU4TI";
const adminID = "5df6318ece7985d52c77b62126a9878d";

//initalize algolia
const client = algolia(appID, adminID);
const index = client.initIndex("Main");

export const indexRecipe = functions.firestore
  .document(`recipes/{recipeID}`)
  .onCreate((snap, context) => {
    const data = snap.data();
    const objectID = snap.id;

    //add data to algolia index
    return index.addObject({
      objectID,
      ...data
    });
  });

export const unindexRecipe = functions.firestore
  .document(`recipes/{recipeID}`)
  .onDelete((snap, context) => {
    const objectID = snap.id;
    //delete from algolia index
    return index.deleteObject(objectID);
  });

export const updateRecipe = functions.firestore
  .document(`recipes/{recipeID}`)
  .onUpdate((snap, context) => {
    const data = snap.after.data();
    const objectID = snap.after.id;

    //update algolia index
    return index.saveObject({
      objectID,
      ...data
    });
  });
