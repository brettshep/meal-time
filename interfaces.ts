export interface Recipe {
  picture: string;
  pictureName?: string;
  title: string;
  cat: string;
  time: string;
  url?: string;
  ingredients?: { text: string }[];
  directions?: string;
  id?: string;
  owner: string;
}

export interface Day {
  breakfast: Recipe[];
  lunch: Recipe[];
  dinner: Recipe[];
  id?: string;
}

export interface RecipeDayInfo {
  [id: string]: {
    dayId: string;
    cat: string;
    recipeID: string;
  };
}

export interface User {
  uid: string;
  email: string;
}
